var express = require('express')
var app = express()
var Manner = require('manner-sdk-js');

const global_config = {
	graphQLURL: 'http://localhost:4444/graphql',
	// graphQLURL: 'https://graphql-dot-manner-production.appspot-preview.com/graphql',
	// graphQLURL: 'https://manner-production.appspot-preview.com/graphql',
	debug: true,
	goalDB: {}
}

function validateHeaders(req) {
	if (!req.headers.token || !req.headers['active-agent']) {
		return false;
	}
	return true;
}

function respond(req, res, thunk) {
	if (validateHeaders(req)) {
		const headers = {
			token: req.headers.token,
			agentID: req.headers['active-agent']
		}
		thunk(headers)
			.then( result => res.json(result) )
			.catch( e => {
				if (e.statusCode) {
					res.status(e.statusCode).json(e.error);
				} else {
					console.error(e)
					res.status(500).send("Unexpected error");
				}
			})
	} else {
		res.status(400).send("Invalid headers");
	}
}

app.get('/logIncoming/:userId', function (req, res) {
	respond(req, res, (headers) => {
		return Manner.logIncoming(req.params.userId, name, req.query.content, headers)
	})
} )

app.get('/logOutgoing/:userId', function (req, res) {
	respond(req, res, (headers) => {
		return Manner.logOutgoing(req.params.userId, name, req.query.content, headers)
	})
} )

app.get('/userContext/:userId', function (req, res) {
	respond(req, res, (headers) => {
		let name, value;
		for (let n in req.query) {
			name = n;
			value = req.query[n];
		}
		return Manner.userContext(req.params.userId, name, value, headers)
	})
} )

app.get('/context/:userId', function (req, res) {
	respond(req, res, (headers) => {
		let name, value;
		for (let n in req.query) {
			name = n;
			value = req.query[n];
		}
		return Manner.context(req.params.userId, name, value, headers)
	})
} )

app.get('/goalFail/:userId/:testSlug', function (req, res) {
	respond(req, res, (headers) => {
		return Manner.goalFail(req.params.userId, req.params.testSlug, headers)
	})
} )

app.get('/goalSuccess/:userId/:testSlug', function (req, res) {
	respond(req, res, (headers) => {
		return Manner.goalSuccess(req.params.userId, req.params.testSlug, headers)
	})
} )

app.get('/startGoal/:userId/:testSlug', function (req, res) {
	respond(req, res, (headers) => {
		return Manner.startGoal(req.params.userId, req.params.testSlug, headers)
	})
} )

app.get('/getResponse/:userId/:testSlug', function (req, res) {
	respond(req, res, (headers) => {
		return Manner.getResponse(req.params.userId, req.params.testSlug, req.query.fallback, headers)
	})
} )

app.get('/getAction/:userId/:testSlug', function (req, res) {
	respond(req, res, (headers) => {
		return Manner.getAction(req.params.userId, req.params.testSlug, headers)
	})
} )

app.use('/getTestResponse/:userId/:testSlug', function (req, res) {
	console.log("hit")
	respond(req, res, (headers) => {
		return Manner.getTestResponse(req.params.userId, req.params.testSlug, headers)
	})
} )


app.listen(3000, function () {
  console.log('Manner REST API listening on port 3000!')
})
